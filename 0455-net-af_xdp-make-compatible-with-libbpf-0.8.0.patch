From 1eb1846b1a2caaf77d0697747d3ecd1272ce0bfc Mon Sep 17 00:00:00 2001
From: Ciara Loftus <ciara.loftus@intel.com>
Date: Thu, 6 Oct 2022 09:26:54 +0300
Subject: [PATCH] net/af_xdp: make compatible with libbpf 0.8.0

[ upstream commit 1eb1846b1a2caaf77d0697747d3ecd1272ce0bfc ]

libbpf v0.8.0 deprecates the bpf_get_link_xdp_id() and
bpf_set_link_xdp_fd() functions. Use meson to detect if
bpf_xdp_attach() is available and if so, use the recommended
replacement functions bpf_xdp_query_id(), bpf_xdp_attach()
and bpf_xdp_detach().

Signed-off-by: Ciara Loftus <ciara.loftus@intel.com>
Signed-off-by: Andrew Rybchenko <andrew.rybchenko@oktetlabs.ru>
Reviewed-by: Ferruh Yigit <ferruh.yigit@xilinx.com>
---
 drivers/net/af_xdp/meson.build         |  5 ++++
 drivers/net/af_xdp/rte_eth_af_xdp.c    | 38 +++++++++++++++++++++++++-
 3 files changed, 46 insertions(+), 1 deletion(-)

diff --git a/drivers/net/af_xdp/meson.build b/drivers/net/af_xdp/meson.build
index 9d5ffab96b..858047989e 100644
--- a/drivers/net/af_xdp/meson.build
+++ b/drivers/net/af_xdp/meson.build
@@ -64,4 +64,9 @@ if build
                      dependencies : bpf_dep)
       cflags += ['-DRTE_NET_AF_XDP_LIBBPF_OBJ_OPEN']
   endif
+  if cc.has_function('bpf_xdp_attach',
+                     prefix : '#include <bpf/libbpf.h>',
+                     dependencies : bpf_dep)
+      cflags += ['-DRTE_NET_AF_XDP_LIBBPF_XDP_ATTACH']
+  endif
 endif
diff --git a/drivers/net/af_xdp/rte_eth_af_xdp.c b/drivers/net/af_xdp/rte_eth_af_xdp.c
index f7c2321a18..b6ec9bf490 100644
--- a/drivers/net/af_xdp/rte_eth_af_xdp.c
+++ b/drivers/net/af_xdp/rte_eth_af_xdp.c
@@ -866,6 +866,40 @@ eth_stats_reset(struct rte_eth_dev *dev)
 	return 0;
 }
 
+#ifdef RTE_NET_AF_XDP_LIBBPF_XDP_ATTACH
+
+static int link_xdp_prog_with_dev(int ifindex, int fd, __u32 flags)
+{
+	return bpf_xdp_attach(ifindex, fd, flags, NULL);
+}
+
+static int
+remove_xdp_program(struct pmd_internals *internals)
+{
+	uint32_t curr_prog_id = 0;
+	int ret;
+
+	ret = bpf_xdp_query_id(internals->if_index, XDP_FLAGS_UPDATE_IF_NOEXIST,
+			       &curr_prog_id);
+	if (ret != 0) {
+		AF_XDP_LOG(ERR, "bpf_xdp_query_id failed\n");
+		return ret;
+	}
+
+	ret = bpf_xdp_detach(internals->if_index, XDP_FLAGS_UPDATE_IF_NOEXIST,
+			     NULL);
+	if (ret != 0)
+		AF_XDP_LOG(ERR, "bpf_xdp_detach failed\n");
+	return ret;
+}
+
+#else
+
+static int link_xdp_prog_with_dev(int ifindex, int fd, __u32 flags)
+{
+	return bpf_set_link_xdp_fd(ifindex, fd, flags);
+}
+
 static int
 remove_xdp_program(struct pmd_internals *internals)
 {
@@ -886,6 +920,8 @@ remove_xdp_program(struct pmd_internals *internals)
 	return ret;
 }
 
+#endif
+
 static void
 xdp_umem_destroy(struct xsk_umem_info *umem)
 {
@@ -1205,7 +1241,7 @@ load_custom_xdp_prog(const char *prog_path, int if_index, struct bpf_map **map)
 	}
 
 	/* Link the program with the given network device */
-	ret = bpf_set_link_xdp_fd(if_index, prog_fd,
+	ret = link_xdp_prog_with_dev(if_index, prog_fd,
 					XDP_FLAGS_UPDATE_IF_NOEXIST);
 	if (ret) {
 		AF_XDP_LOG(ERR, "Failed to set prog fd %d on interface\n",
-- 
2.33.0

