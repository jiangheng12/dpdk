From 2d42f6d26a83262c9765c6db7dfca44c8c26d995 Mon Sep 17 00:00:00 2001
From: Chengwen Feng <fengchengwen@huawei.com>
Date: Mon, 6 Feb 2023 10:53:10 +0000
Subject: mem: fix hugepage info mapping

[ upstream commit 66e7ac416f3d62c5ee773ff02c115a24da9f1991 ]

The map_shared_memory() function should treat mmap MAP_FAILED as NULL
because callers compare it with NULL to determine whether the map is
failed.

Fixes: 764bf26873b9 ("add FreeBSD support")
Fixes: cb97d93e9d3b ("mem: share hugepage info primary and secondary")
Cc: stable@dpdk.org

Signed-off-by: Chengwen Feng <fengchengwen@huawei.com>
Reviewed-by: David Marchand <david.marchand@redhat.com>
---
 lib/eal/freebsd/eal_hugepage_info.c | 2 +-
 lib/eal/linux/eal_hugepage_info.c   | 2 +-
 2 files changed, 2 insertions(+), 2 deletions(-)

diff --git a/lib/eal/freebsd/eal_hugepage_info.c b/lib/eal/freebsd/eal_hugepage_info.c
index 9dbe375bd3..e58e618469 100644
--- a/lib/eal/freebsd/eal_hugepage_info.c
+++ b/lib/eal/freebsd/eal_hugepage_info.c
@@ -33,7 +33,7 @@ map_shared_memory(const char *filename, const size_t mem_size, int flags)
 	}
 	retval = mmap(NULL, mem_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
 	close(fd);
-	return retval;
+	return retval == MAP_FAILED ? NULL : retval;
 }
 
 static void *
diff --git a/lib/eal/linux/eal_hugepage_info.c b/lib/eal/linux/eal_hugepage_info.c
index 41acf180ee..6c76a36a0d 100644
--- a/lib/eal/linux/eal_hugepage_info.c
+++ b/lib/eal/linux/eal_hugepage_info.c
@@ -57,7 +57,7 @@ map_shared_memory(const char *filename, const size_t mem_size, int flags)
 	retval = mmap(NULL, mem_size, PROT_READ | PROT_WRITE,
 			MAP_SHARED, fd, 0);
 	close(fd);
-	return retval;
+	return retval == MAP_FAILED ? NULL : retval;
 }
 
 static void *
-- 
2.23.0

