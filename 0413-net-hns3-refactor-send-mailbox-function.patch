From f87d448eb2105d7be30bef7a01fc6182e846a4fd Mon Sep 17 00:00:00 2001
From: Dengdui Huang <huangdengdui@huawei.com>
Date: Fri, 8 Dec 2023 14:55:07 +0800
Subject: [PATCH 413/418] net/hns3: refactor send mailbox function

[ upstream commit 15aab7018e0fcdfd9a4429078c5d9edcfda7c570 ]

The 'hns3_send_mbx_msg' function has following problem:
1. the name is vague, missing caller indication
2. too many input parameters because the filling messages
   are placed in commands the send command.

Therefore, a common interface is encapsulated to fill in
the mailbox message before sending it.

Fixes: 463e748964f5 ("net/hns3: support mailbox")
Cc: stable@dpdk.org

Signed-off-by: Dengdui Huang <huangdengdui@huawei.com>
Signed-off-by: Jie Hai <haijie1@huawei.com>
---
 drivers/net/hns3/hns3_ethdev_vf.c | 141 ++++++++++++++++++------------
 drivers/net/hns3/hns3_mbx.c       |  50 ++++-------
 drivers/net/hns3/hns3_mbx.h       |   8 +-
 drivers/net/hns3/hns3_rxtx.c      |  18 ++--
 4 files changed, 116 insertions(+), 101 deletions(-)

diff --git a/drivers/net/hns3/hns3_ethdev_vf.c b/drivers/net/hns3/hns3_ethdev_vf.c
index 83dacbaec7..ef294cf03d 100644
--- a/drivers/net/hns3/hns3_ethdev_vf.c
+++ b/drivers/net/hns3/hns3_ethdev_vf.c
@@ -172,11 +172,13 @@ hns3vf_add_uc_mac_addr(struct hns3_hw *hw, struct rte_ether_addr *mac_addr)
 {
 	/* mac address was checked by upper level interface */
 	char mac_str[RTE_ETHER_ADDR_FMT_SIZE];
+	struct hns3_vf_to_pf_msg req;
 	int ret;
 
-	ret = hns3_send_mbx_msg(hw, HNS3_MBX_SET_UNICAST,
-				HNS3_MBX_MAC_VLAN_UC_ADD, mac_addr->addr_bytes,
-				RTE_ETHER_ADDR_LEN, false, NULL, 0);
+	hns3vf_mbx_setup(&req, HNS3_MBX_SET_UNICAST,
+			 HNS3_MBX_MAC_VLAN_UC_ADD);
+	memcpy(req.data, mac_addr->addr_bytes, RTE_ETHER_ADDR_LEN);
+	ret = hns3vf_mbx_send(hw, &req, false, NULL, 0);
 	if (ret) {
 		hns3_ether_format_addr(mac_str, RTE_ETHER_ADDR_FMT_SIZE,
 				      mac_addr);
@@ -191,12 +193,13 @@ hns3vf_remove_uc_mac_addr(struct hns3_hw *hw, struct rte_ether_addr *mac_addr)
 {
 	/* mac address was checked by upper level interface */
 	char mac_str[RTE_ETHER_ADDR_FMT_SIZE];
+	struct hns3_vf_to_pf_msg req;
 	int ret;
 
-	ret = hns3_send_mbx_msg(hw, HNS3_MBX_SET_UNICAST,
-				HNS3_MBX_MAC_VLAN_UC_REMOVE,
-				mac_addr->addr_bytes, RTE_ETHER_ADDR_LEN,
-				false, NULL, 0);
+	hns3vf_mbx_setup(&req, HNS3_MBX_SET_UNICAST,
+			 HNS3_MBX_MAC_VLAN_UC_REMOVE);
+	memcpy(req.data, mac_addr->addr_bytes, RTE_ETHER_ADDR_LEN);
+	ret = hns3vf_mbx_send(hw, &req, false, NULL, 0);
 	if (ret) {
 		hns3_ether_format_addr(mac_str, RTE_ETHER_ADDR_FMT_SIZE,
 				       mac_addr);
@@ -215,6 +218,7 @@ hns3vf_set_default_mac_addr(struct rte_eth_dev *dev,
 	struct rte_ether_addr *old_addr;
 	uint8_t addr_bytes[HNS3_TWO_ETHER_ADDR_LEN]; /* for 2 MAC addresses */
 	char mac_str[RTE_ETHER_ADDR_FMT_SIZE];
+	struct hns3_vf_to_pf_msg req;
 	int ret;
 
 	/*
@@ -227,9 +231,10 @@ hns3vf_set_default_mac_addr(struct rte_eth_dev *dev,
 	memcpy(&addr_bytes[RTE_ETHER_ADDR_LEN], old_addr->addr_bytes,
 	       RTE_ETHER_ADDR_LEN);
 
-	ret = hns3_send_mbx_msg(hw, HNS3_MBX_SET_UNICAST,
-				HNS3_MBX_MAC_VLAN_UC_MODIFY, addr_bytes,
-				HNS3_TWO_ETHER_ADDR_LEN, true, NULL, 0);
+	hns3vf_mbx_setup(&req, HNS3_MBX_SET_UNICAST,
+			 HNS3_MBX_MAC_VLAN_UC_MODIFY);
+	memcpy(req.data, addr_bytes, HNS3_TWO_ETHER_ADDR_LEN);
+	ret = hns3vf_mbx_send(hw, &req, true, NULL, 0);
 	if (ret) {
 		/*
 		 * The hns3 VF PMD depends on the hns3 PF kernel ethdev
@@ -266,12 +271,13 @@ hns3vf_add_mc_mac_addr(struct hns3_hw *hw,
 		       struct rte_ether_addr *mac_addr)
 {
 	char mac_str[RTE_ETHER_ADDR_FMT_SIZE];
+	struct hns3_vf_to_pf_msg req;
 	int ret;
 
-	ret = hns3_send_mbx_msg(hw, HNS3_MBX_SET_MULTICAST,
-				HNS3_MBX_MAC_VLAN_MC_ADD,
-				mac_addr->addr_bytes, RTE_ETHER_ADDR_LEN, false,
-				NULL, 0);
+	hns3vf_mbx_setup(&req, HNS3_MBX_SET_MULTICAST,
+			 HNS3_MBX_MAC_VLAN_MC_ADD);
+	memcpy(req.data, mac_addr->addr_bytes, RTE_ETHER_ADDR_LEN);
+	ret = hns3vf_mbx_send(hw, &req, false, NULL, 0);
 	if (ret) {
 		hns3_ether_format_addr(mac_str, RTE_ETHER_ADDR_FMT_SIZE,
 				      mac_addr);
@@ -287,12 +293,13 @@ hns3vf_remove_mc_mac_addr(struct hns3_hw *hw,
 			  struct rte_ether_addr *mac_addr)
 {
 	char mac_str[RTE_ETHER_ADDR_FMT_SIZE];
+	struct hns3_vf_to_pf_msg req;
 	int ret;
 
-	ret = hns3_send_mbx_msg(hw, HNS3_MBX_SET_MULTICAST,
-				HNS3_MBX_MAC_VLAN_MC_REMOVE,
-				mac_addr->addr_bytes, RTE_ETHER_ADDR_LEN, false,
-				NULL, 0);
+	hns3vf_mbx_setup(&req, HNS3_MBX_SET_MULTICAST,
+			 HNS3_MBX_MAC_VLAN_MC_REMOVE);
+	memcpy(req.data, mac_addr->addr_bytes, RTE_ETHER_ADDR_LEN);
+	ret = hns3vf_mbx_send(hw, &req, false, NULL, 0);
 	if (ret) {
 		hns3_ether_format_addr(mac_str, RTE_ETHER_ADDR_FMT_SIZE,
 				       mac_addr);
@@ -429,7 +436,6 @@ hns3vf_bind_ring_with_vector(struct hns3_hw *hw, uint16_t vector_id,
 			     bool mmap, enum hns3_ring_type queue_type,
 			     uint16_t queue_id)
 {
-#define HNS3_RING_VERCTOR_DATA_SIZE	14
 	struct hns3_vf_to_pf_msg req = {0};
 	const char *op_str;
 	int ret;
@@ -446,8 +452,7 @@ hns3vf_bind_ring_with_vector(struct hns3_hw *hw, uint16_t vector_id,
 	req.ring_param[0].ring_type = queue_type;
 	req.ring_param[0].tqp_index = queue_id;
 	op_str = mmap ? "Map" : "Unmap";
-	ret = hns3_send_mbx_msg(hw, req.code, 0, (uint8_t *)&req.vector_id,
-				HNS3_RING_VERCTOR_DATA_SIZE, false, NULL, 0);
+	ret = hns3vf_mbx_send(hw, &req, false, NULL, 0);
 	if (ret)
 		hns3_err(hw, "%s TQP %u fail, vector_id is %u, ret = %d.",
 			 op_str, queue_id, req.vector_id, ret);
@@ -533,10 +538,12 @@ hns3vf_dev_configure(struct rte_eth_dev *dev)
 static int
 hns3vf_config_mtu(struct hns3_hw *hw, uint16_t mtu)
 {
+	struct hns3_vf_to_pf_msg req;
 	int ret;
 
-	ret = hns3_send_mbx_msg(hw, HNS3_MBX_SET_MTU, 0, (const uint8_t *)&mtu,
-				sizeof(mtu), true, NULL, 0);
+	hns3vf_mbx_setup(&req, HNS3_MBX_SET_MTU, 0);
+	memcpy(req.data, &mtu, sizeof(mtu));
+	ret = hns3vf_mbx_send(hw, &req, true, NULL, 0);
 	if (ret)
 		hns3_err(hw, "Failed to set mtu (%u) for vf: %d", mtu, ret);
 
@@ -712,12 +719,13 @@ hns3vf_get_push_lsc_cap(struct hns3_hw *hw)
 	uint16_t val = HNS3_PF_PUSH_LSC_CAP_NOT_SUPPORTED;
 	uint16_t exp = HNS3_PF_PUSH_LSC_CAP_UNKNOWN;
 	struct hns3_vf *vf = HNS3_DEV_HW_TO_VF(hw);
+	struct hns3_vf_to_pf_msg req;
 
 	__atomic_store_n(&vf->pf_push_lsc_cap, HNS3_PF_PUSH_LSC_CAP_UNKNOWN,
 			 __ATOMIC_RELEASE);
 
-	(void)hns3_send_mbx_msg(hw, HNS3_MBX_GET_LINK_STATUS, 0, NULL, 0, false,
-				NULL, 0);
+	hns3vf_mbx_setup(&req, HNS3_MBX_GET_LINK_STATUS, 0);
+	(void)hns3vf_mbx_send(hw, &req, false, NULL, 0);
 
 	while (remain_ms > 0) {
 		rte_delay_ms(HNS3_POLL_RESPONE_MS);
@@ -812,12 +820,13 @@ hns3vf_check_tqp_info(struct hns3_hw *hw)
 static int
 hns3vf_get_port_base_vlan_filter_state(struct hns3_hw *hw)
 {
+	struct hns3_vf_to_pf_msg req;
 	uint8_t resp_msg;
 	int ret;
 
-	ret = hns3_send_mbx_msg(hw, HNS3_MBX_SET_VLAN,
-				HNS3_MBX_GET_PORT_BASE_VLAN_STATE, NULL, 0,
-				true, &resp_msg, sizeof(resp_msg));
+	hns3vf_mbx_setup(&req, HNS3_MBX_SET_VLAN,
+			 HNS3_MBX_GET_PORT_BASE_VLAN_STATE);
+	ret = hns3vf_mbx_send(hw, &req, true, &resp_msg, sizeof(resp_msg));
 	if (ret) {
 		if (ret == -ETIME) {
 			/*
@@ -858,10 +867,12 @@ hns3vf_get_queue_info(struct hns3_hw *hw)
 {
 #define HNS3VF_TQPS_RSS_INFO_LEN	6
 	uint8_t resp_msg[HNS3VF_TQPS_RSS_INFO_LEN];
+	struct hns3_vf_to_pf_msg req;
 	int ret;
 
-	ret = hns3_send_mbx_msg(hw, HNS3_MBX_GET_QINFO, 0, NULL, 0, true,
-				resp_msg, HNS3VF_TQPS_RSS_INFO_LEN);
+	hns3vf_mbx_setup(&req, HNS3_MBX_GET_QINFO, 0);
+	ret = hns3vf_mbx_send(hw, &req, true,
+			      resp_msg, HNS3VF_TQPS_RSS_INFO_LEN);
 	if (ret) {
 		PMD_INIT_LOG(ERR, "Failed to get tqp info from PF: %d", ret);
 		return ret;
@@ -899,10 +910,11 @@ hns3vf_get_basic_info(struct hns3_hw *hw)
 {
 	uint8_t resp_msg[HNS3_MBX_MAX_RESP_DATA_SIZE];
 	struct hns3_basic_info *basic_info;
+	struct hns3_vf_to_pf_msg req;
 	int ret;
 
-	ret = hns3_send_mbx_msg(hw, HNS3_MBX_GET_BASIC_INFO, 0, NULL, 0,
-				true, resp_msg, sizeof(resp_msg));
+	hns3vf_mbx_setup(&req, HNS3_MBX_GET_BASIC_INFO, 0);
+	ret = hns3vf_mbx_send(hw, &req, true, resp_msg, sizeof(resp_msg));
 	if (ret) {
 		hns3_err(hw, "failed to get basic info from PF, ret = %d.",
 				ret);
@@ -922,10 +934,11 @@ static int
 hns3vf_get_host_mac_addr(struct hns3_hw *hw)
 {
 	uint8_t host_mac[RTE_ETHER_ADDR_LEN];
+	struct hns3_vf_to_pf_msg req;
 	int ret;
 
-	ret = hns3_send_mbx_msg(hw, HNS3_MBX_GET_MAC_ADDR, 0, NULL, 0,
-				true, host_mac, RTE_ETHER_ADDR_LEN);
+	hns3vf_mbx_setup(&req, HNS3_MBX_GET_MAC_ADDR, 0);
+	ret = hns3vf_mbx_send(hw, &req, true, host_mac, RTE_ETHER_ADDR_LEN);
 	if (ret) {
 		hns3_err(hw, "Failed to get mac addr from PF: %d", ret);
 		return ret;
@@ -974,6 +987,7 @@ static void
 hns3vf_request_link_info(struct hns3_hw *hw)
 {
 	struct hns3_vf *vf = HNS3_DEV_HW_TO_VF(hw);
+	struct hns3_vf_to_pf_msg req;
 	bool send_req;
 	int ret;
 
@@ -985,8 +999,8 @@ hns3vf_request_link_info(struct hns3_hw *hw)
 	if (!send_req)
 		return;
 
-	ret = hns3_send_mbx_msg(hw, HNS3_MBX_GET_LINK_STATUS, 0, NULL, 0, false,
-				NULL, 0);
+	hns3vf_mbx_setup(&req, HNS3_MBX_GET_LINK_STATUS, 0);
+	ret = hns3vf_mbx_send(hw, &req, false, NULL, 0);
 	if (ret) {
 		hns3_err(hw, "failed to fetch link status, ret = %d", ret);
 		return;
@@ -1030,16 +1044,18 @@ hns3vf_update_link_status(struct hns3_hw *hw, uint8_t link_status,
 static int
 hns3vf_vlan_filter_configure(struct hns3_adapter *hns, uint16_t vlan_id, int on)
 {
-	struct hns3_mbx_vlan_filter vlan_filter = {0};
+	struct hns3_mbx_vlan_filter *vlan_filter;
+	struct hns3_vf_to_pf_msg req = {0};
 	struct hns3_hw *hw = &hns->hw;
 
-	vlan_filter.is_kill = on ? 0 : 1;
-	vlan_filter.proto = rte_cpu_to_le_16(RTE_ETHER_TYPE_VLAN);
-	vlan_filter.vlan_id =  rte_cpu_to_le_16(vlan_id);
+	req.code = HNS3_MBX_SET_VLAN;
+	req.subcode = HNS3_MBX_VLAN_FILTER;
+	vlan_filter = (struct hns3_mbx_vlan_filter *)req.data;
+	vlan_filter->is_kill = on ? 0 : 1;
+	vlan_filter->proto = rte_cpu_to_le_16(RTE_ETHER_TYPE_VLAN);
+	vlan_filter->vlan_id = rte_cpu_to_le_16(vlan_id);
 
-	return hns3_send_mbx_msg(hw, HNS3_MBX_SET_VLAN, HNS3_MBX_VLAN_FILTER,
-				(uint8_t *)&vlan_filter, sizeof(vlan_filter),
-				 true, NULL, 0);
+	return hns3vf_mbx_send(hw, &req, true, NULL, 0);
 }
 
 static int
@@ -1068,6 +1084,7 @@ hns3vf_vlan_filter_set(struct rte_eth_dev *dev, uint16_t vlan_id, int on)
 static int
 hns3vf_en_vlan_filter(struct hns3_hw *hw, bool enable)
 {
+	struct hns3_vf_to_pf_msg req;
 	uint8_t msg_data;
 	int ret;
 
@@ -1075,9 +1092,10 @@ hns3vf_en_vlan_filter(struct hns3_hw *hw, bool enable)
 		return 0;
 
 	msg_data = enable ? 1 : 0;
-	ret = hns3_send_mbx_msg(hw, HNS3_MBX_SET_VLAN,
-			HNS3_MBX_ENABLE_VLAN_FILTER, &msg_data,
-			sizeof(msg_data), true, NULL, 0);
+	hns3vf_mbx_setup(&req, HNS3_MBX_SET_VLAN,
+			 HNS3_MBX_ENABLE_VLAN_FILTER);
+	memcpy(req.data, &msg_data, sizeof(msg_data));
+	ret = hns3vf_mbx_send(hw, &req, true, NULL, 0);
 	if (ret)
 		hns3_err(hw, "%s vlan filter failed, ret = %d.",
 				enable ? "enable" : "disable", ret);
@@ -1088,12 +1106,15 @@ hns3vf_en_vlan_filter(struct hns3_hw *hw, bool enable)
 static int
 hns3vf_en_hw_strip_rxvtag(struct hns3_hw *hw, bool enable)
 {
+	struct hns3_vf_to_pf_msg req;
 	uint8_t msg_data;
 	int ret;
 
 	msg_data = enable ? 1 : 0;
-	ret = hns3_send_mbx_msg(hw, HNS3_MBX_SET_VLAN, HNS3_MBX_VLAN_RX_OFF_CFG,
-				&msg_data, sizeof(msg_data), false, NULL, 0);
+	hns3vf_mbx_setup(&req, HNS3_MBX_SET_VLAN,
+			 HNS3_MBX_VLAN_RX_OFF_CFG);
+	memcpy(req.data, &msg_data, sizeof(msg_data));
+	ret = hns3vf_mbx_send(hw, &req, false, NULL, 0);
 	if (ret)
 		hns3_err(hw, "vf %s strip failed, ret = %d.",
 				enable ? "enable" : "disable", ret);
@@ -1237,11 +1258,13 @@ hns3vf_dev_configure_vlan(struct rte_eth_dev *dev)
 static int
 hns3vf_set_alive(struct hns3_hw *hw, bool alive)
 {
+	struct hns3_vf_to_pf_msg req;
 	uint8_t msg_data;
 
 	msg_data = alive ? 1 : 0;
-	return hns3_send_mbx_msg(hw, HNS3_MBX_SET_ALIVE, 0, &msg_data,
-				 sizeof(msg_data), false, NULL, 0);
+	hns3vf_mbx_setup(&req, HNS3_MBX_SET_ALIVE, 0);
+	memcpy(req.data, &msg_data, sizeof(msg_data));
+	return hns3vf_mbx_send(hw, &req, false, NULL, 0);
 }
 
 static void
@@ -1249,11 +1272,12 @@ hns3vf_keep_alive_handler(void *param)
 {
 	struct rte_eth_dev *eth_dev = (struct rte_eth_dev *)param;
 	struct hns3_adapter *hns = eth_dev->data->dev_private;
+	struct hns3_vf_to_pf_msg req;
 	struct hns3_hw *hw = &hns->hw;
 	int ret;
 
-	ret = hns3_send_mbx_msg(hw, HNS3_MBX_KEEP_ALIVE, 0, NULL, 0,
-				false, NULL, 0);
+	hns3vf_mbx_setup(&req, HNS3_MBX_KEEP_ALIVE, 0);
+	ret = hns3vf_mbx_send(hw, &req, false, NULL, 0);
 	if (ret)
 		hns3_err(hw, "VF sends keeping alive cmd failed(=%d)",
 			 ret);
@@ -1392,9 +1416,11 @@ hns3vf_init_hardware(struct hns3_adapter *hns)
 static int
 hns3vf_clear_vport_list(struct hns3_hw *hw)
 {
-	return hns3_send_mbx_msg(hw, HNS3_MBX_HANDLE_VF_TBL,
-				 HNS3_MBX_VPORT_LIST_CLEAR, NULL, 0, false,
-				 NULL, 0);
+	struct hns3_vf_to_pf_msg req;
+
+	hns3vf_mbx_setup(&req, HNS3_MBX_HANDLE_VF_TBL,
+			 HNS3_MBX_VPORT_LIST_CLEAR);
+	return hns3vf_mbx_send(hw, &req, false, NULL, 0);
 }
 
 static int
@@ -1863,12 +1889,13 @@ hns3vf_wait_hardware_ready(struct hns3_adapter *hns)
 static int
 hns3vf_prepare_reset(struct hns3_adapter *hns)
 {
+	struct hns3_vf_to_pf_msg req;
 	struct hns3_hw *hw = &hns->hw;
 	int ret;
 
 	if (hw->reset.level == HNS3_VF_FUNC_RESET) {
-		ret = hns3_send_mbx_msg(hw, HNS3_MBX_RESET, 0, NULL,
-					0, true, NULL, 0);
+		hns3vf_mbx_setup(&req, HNS3_MBX_RESET, 0);
+		ret = hns3vf_mbx_send(hw, &req, true, NULL, 0);
 		if (ret)
 			return ret;
 	}
diff --git a/drivers/net/hns3/hns3_mbx.c b/drivers/net/hns3/hns3_mbx.c
index c90f5d59ba..43195ff184 100644
--- a/drivers/net/hns3/hns3_mbx.c
+++ b/drivers/net/hns3/hns3_mbx.c
@@ -24,6 +24,14 @@ static const struct errno_respcode_map err_code_map[] = {
 	{95, -EOPNOTSUPP},
 };
 
+void
+hns3vf_mbx_setup(struct hns3_vf_to_pf_msg *req, uint8_t code, uint8_t subcode)
+{
+	memset(req, 0, sizeof(struct hns3_vf_to_pf_msg));
+	req->code = code;
+	req->subcode = subcode;
+}
+
 static int
 hns3_resp_to_errno(uint16_t resp_code)
 {
@@ -118,45 +126,24 @@ hns3_mbx_prepare_resp(struct hns3_hw *hw, uint16_t code, uint16_t subcode)
 }
 
 int
-hns3_send_mbx_msg(struct hns3_hw *hw, uint16_t code, uint16_t subcode,
-		  const uint8_t *msg_data, uint8_t msg_len, bool need_resp,
-		  uint8_t *resp_data, uint16_t resp_len)
+hns3vf_mbx_send(struct hns3_hw *hw,
+		struct hns3_vf_to_pf_msg *req, bool need_resp,
+		uint8_t *resp_data, uint16_t resp_len)
 {
-	struct hns3_mbx_vf_to_pf_cmd *req;
+	struct hns3_mbx_vf_to_pf_cmd *cmd;
 	struct hns3_cmd_desc desc;
-	bool is_ring_vector_msg;
 	int ret;
 
-	req = (struct hns3_mbx_vf_to_pf_cmd *)desc.data;
-
-	/* first two bytes are reserved for code & subcode */
-	if (msg_len > HNS3_MBX_MSG_MAX_DATA_SIZE) {
-		hns3_err(hw,
-			 "VF send mbx msg fail, msg len %u exceeds max payload len %d",
-			 msg_len, HNS3_MBX_MSG_MAX_DATA_SIZE);
-		return -EINVAL;
-	}
-
 	hns3_cmd_setup_basic_desc(&desc, HNS3_OPC_MBX_VF_TO_PF, false);
-	req->msg.code = code;
-	is_ring_vector_msg = (code == HNS3_MBX_MAP_RING_TO_VECTOR) ||
-			     (code == HNS3_MBX_UNMAP_RING_TO_VECTOR) ||
-			     (code == HNS3_MBX_GET_RING_VECTOR_MAP);
-	if (!is_ring_vector_msg)
-		req->msg.subcode = subcode;
-	if (msg_data) {
-		if (is_ring_vector_msg)
-			memcpy(&req->msg.vector_id, msg_data, msg_len);
-		else
-			memcpy(&req->msg.data, msg_data, msg_len);
-	}
+	cmd = (struct hns3_mbx_vf_to_pf_cmd *)desc.data;
+	cmd->msg = *req;
 
 	/* synchronous send */
 	if (need_resp) {
-		req->mbx_need_resp |= HNS3_MBX_NEED_RESP_BIT;
+		cmd->mbx_need_resp |= HNS3_MBX_NEED_RESP_BIT;
 		rte_spinlock_lock(&hw->mbx_resp.lock);
-		hns3_mbx_prepare_resp(hw, code, subcode);
-		req->match_id = hw->mbx_resp.match_id;
+		hns3_mbx_prepare_resp(hw, req->code, req->subcode);
+		cmd->match_id = hw->mbx_resp.match_id;
 		ret = hns3_cmd_send(hw, &desc, 1);
 		if (ret) {
 			rte_spinlock_unlock(&hw->mbx_resp.lock);
@@ -165,7 +152,8 @@ hns3_send_mbx_msg(struct hns3_hw *hw, uint16_t code, uint16_t subcode,
 			return ret;
 		}
 
-		ret = hns3_get_mbx_resp(hw, code, subcode, resp_data, resp_len);
+		ret = hns3_get_mbx_resp(hw, req->code, req->subcode,
+					resp_data, resp_len);
 		rte_spinlock_unlock(&hw->mbx_resp.lock);
 	} else {
 		/* asynchronous send */
diff --git a/drivers/net/hns3/hns3_mbx.h b/drivers/net/hns3/hns3_mbx.h
index 09780fcebd..2952b96ab6 100644
--- a/drivers/net/hns3/hns3_mbx.h
+++ b/drivers/net/hns3/hns3_mbx.h
@@ -208,7 +208,9 @@ struct hns3_pf_rst_done_cmd {
 
 struct hns3_hw;
 void hns3_dev_handle_mbx_msg(struct hns3_hw *hw);
-int hns3_send_mbx_msg(struct hns3_hw *hw, uint16_t code, uint16_t subcode,
-		      const uint8_t *msg_data, uint8_t msg_len, bool need_resp,
-		      uint8_t *resp_data, uint16_t resp_len);
+void hns3vf_mbx_setup(struct hns3_vf_to_pf_msg *req,
+		      uint8_t code, uint8_t subcode);
+int hns3vf_mbx_send(struct hns3_hw *hw,
+		    struct hns3_vf_to_pf_msg *req_msg, bool need_resp,
+		    uint8_t *resp_data, uint16_t resp_len);
 #endif /* HNS3_MBX_H */
diff --git a/drivers/net/hns3/hns3_rxtx.c b/drivers/net/hns3/hns3_rxtx.c
index 8b7c469685..81edd5258d 100644
--- a/drivers/net/hns3/hns3_rxtx.c
+++ b/drivers/net/hns3/hns3_rxtx.c
@@ -686,13 +686,12 @@ hns3pf_reset_tqp(struct hns3_hw *hw, uint16_t queue_id)
 static int
 hns3vf_reset_tqp(struct hns3_hw *hw, uint16_t queue_id)
 {
-	uint8_t msg_data[2];
+	struct hns3_vf_to_pf_msg req;
 	int ret;
 
-	memcpy(msg_data, &queue_id, sizeof(uint16_t));
-
-	ret = hns3_send_mbx_msg(hw, HNS3_MBX_QUEUE_RESET, 0, msg_data,
-				 sizeof(msg_data), true, NULL, 0);
+	hns3vf_mbx_setup(&req, HNS3_MBX_QUEUE_RESET, 0);
+	memcpy(req.data, &queue_id, sizeof(uint16_t));
+	ret = hns3vf_mbx_send(hw, &req, true, NULL, 0);
 	if (ret)
 		hns3_err(hw, "fail to reset tqp, queue_id = %u, ret = %d.",
 			 queue_id, ret);
@@ -769,15 +768,14 @@ static int
 hns3vf_reset_all_tqps(struct hns3_hw *hw)
 {
 #define HNS3VF_RESET_ALL_TQP_DONE	1U
+	struct hns3_vf_to_pf_msg req;
 	uint8_t reset_status;
-	uint8_t msg_data[2];
 	int ret;
 	uint16_t i;
 
-	memset(msg_data, 0, sizeof(msg_data));
-	ret = hns3_send_mbx_msg(hw, HNS3_MBX_QUEUE_RESET, 0, msg_data,
-				sizeof(msg_data), true, &reset_status,
-				sizeof(reset_status));
+	hns3vf_mbx_setup(&req, HNS3_MBX_QUEUE_RESET, 0);
+	ret = hns3vf_mbx_send(hw, &req, true,
+			      &reset_status, sizeof(reset_status));
 	if (ret) {
 		hns3_err(hw, "fail to send rcb reset mbx, ret = %d.", ret);
 		return ret;
-- 
2.41.0.windows.2

