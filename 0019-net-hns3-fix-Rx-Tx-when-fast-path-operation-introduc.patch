From 7ae766e2863868698a26a17f40995bd5ba02356d Mon Sep 17 00:00:00 2001
From: "Min Hu (Connor)" <humin29@huawei.com>
Date: Mon, 17 Jan 2022 10:43:00 +0800
Subject: [PATCH] net/hns3: fix Rx/Tx when fast path operation introduced

When fast path operation is introduced, the Rx/Tx function is done by
object 'rte_eth_fp_ops'. So 'rte_eth_fp_ops' should be updated if
'fast-path functions' need to be changed, such as PMD receive function,
prepare function and so on.

This patch fixed receiving packets bug when fast path operation is
introduced.

Fixes: bba636698316 ("net/hns3: support Rx/Tx and related operations")
Fixes: 168b7d79dada ("net/hns3: support set link up/down for PF")
Cc: stable@dpdk.org

Signed-off-by: Min Hu (Connor) <humin29@huawei.com>
---
 drivers/net/hns3/hns3_mp.c   |  7 ++-----
 drivers/net/hns3/hns3_rxtx.c | 28 +++++++++++++++++++++++++++-
 2 files changed, 29 insertions(+), 6 deletions(-)

diff --git a/drivers/net/hns3/hns3_mp.c b/drivers/net/hns3/hns3_mp.c
index 999b407f7d..e74ddea195 100644
--- a/drivers/net/hns3/hns3_mp.c
+++ b/drivers/net/hns3/hns3_mp.c
@@ -74,7 +74,6 @@ mp_secondary_handle(const struct rte_mp_msg *mp_msg, const void *peer)
 	struct hns3_mp_param *res = (struct hns3_mp_param *)mp_res.param;
 	const struct hns3_mp_param *param =
 		(const struct hns3_mp_param *)mp_msg->param;
-	eth_tx_prep_t prep = NULL;
 	struct rte_eth_dev *dev;
 	int ret;
 
@@ -98,14 +97,12 @@ mp_secondary_handle(const struct rte_mp_msg *mp_msg, const void *peer)
 	case HNS3_MP_REQ_START_TX:
 		PMD_INIT_LOG(INFO, "port %u starting Tx datapath",
 			     dev->data->port_id);
-		dev->tx_pkt_burst = hns3_get_tx_function(dev, &prep);
-		dev->tx_pkt_prepare = prep;
+		hns3_start_tx_datapath(dev);
 		break;
 	case HNS3_MP_REQ_STOP_TX:
 		PMD_INIT_LOG(INFO, "port %u stopping Tx datapath",
 			     dev->data->port_id);
-		dev->tx_pkt_burst = hns3_dummy_rxtx_burst;
-		dev->tx_pkt_prepare = NULL;
+		hns3_stop_tx_datapath(dev);
 		break;
 	default:
 		rte_errno = EINVAL;
diff --git a/drivers/net/hns3/hns3_rxtx.c b/drivers/net/hns3/hns3_rxtx.c
index d240e36e6a..c86aeb2366 100644
--- a/drivers/net/hns3/hns3_rxtx.c
+++ b/drivers/net/hns3/hns3_rxtx.c
@@ -4408,7 +4408,21 @@ hns3_trace_rxtx_function(struct rte_eth_dev *dev)
 		 rx_mode.info, tx_mode.info);
 }
 
-void hns3_set_rxtx_function(struct rte_eth_dev *eth_dev)
+static void
+hns3_eth_dev_fp_ops_config(const struct rte_eth_dev *dev)
+{
+	struct rte_eth_fp_ops *fpo = rte_eth_fp_ops;
+	uint16_t port_id = dev->data->port_id;
+
+	fpo[port_id].rx_pkt_burst = dev->rx_pkt_burst;
+	fpo[port_id].tx_pkt_burst = dev->tx_pkt_burst;
+	fpo[port_id].tx_pkt_prepare = dev->tx_pkt_prepare;
+	fpo[port_id].rx_descriptor_status = dev->rx_descriptor_status;
+	fpo[port_id].tx_descriptor_status = dev->tx_descriptor_status;
+}
+
+void
+hns3_set_rxtx_function(struct rte_eth_dev *eth_dev)
 {
 	struct hns3_hw *hw = HNS3_DEV_PRIVATE_TO_HW(eth_dev->data->dev_private);
 	struct hns3_adapter *hns = eth_dev->data->dev_private;
@@ -4429,6 +4443,8 @@ void hns3_set_rxtx_function(struct rte_eth_dev *eth_dev)
 		eth_dev->tx_pkt_burst = hns3_dummy_rxtx_burst;
 		eth_dev->tx_pkt_prepare = NULL;
 	}
+
+	hns3_eth_dev_fp_ops_config(eth_dev);
 }
 
 void
@@ -4729,6 +4745,11 @@ hns3_stop_tx_datapath(struct rte_eth_dev *dev)
 {
 	dev->tx_pkt_burst = hns3_dummy_rxtx_burst;
 	dev->tx_pkt_prepare = NULL;
+	hns3_eth_dev_fp_ops_config(dev);
+
+	if (rte_eal_process_type() == RTE_PROC_SECONDARY)
+		return;
+
 	rte_wmb();
 	/* Disable tx datapath on secondary process. */
 	hns3_mp_req_stop_tx(dev);
@@ -4743,5 +4764,10 @@ hns3_start_tx_datapath(struct rte_eth_dev *dev)
 
 	dev->tx_pkt_burst = hns3_get_tx_function(dev, &prep);
 	dev->tx_pkt_prepare = prep;
+	hns3_eth_dev_fp_ops_config(dev);
+
+	if (rte_eal_process_type() == RTE_PROC_SECONDARY)
+		return;
+
 	hns3_mp_req_start_tx(dev);
 }
-- 
2.33.0

