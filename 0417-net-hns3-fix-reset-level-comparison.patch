From 6bcb6ad4df79beb4dc097fbf2feb28627ce99083 Mon Sep 17 00:00:00 2001
From: Dengdui Huang <huangdengdui@huawei.com>
Date: Fri, 8 Dec 2023 15:44:16 +0800
Subject: [PATCH 418/418] net/hns3: fix reset level comparison

[ upstream commit 0e929c0f6ba9a9ff56310bec3b2127237b0b08f4 ]

Currently, there are two problems in hns3vf_is_reset_pending():
1. The new detect reset level is not HNS3_NONE_RESET, but the
   last reset level is HNS3_NONE_RESET, this function returns false.
2. Comparison between last_req and new_req is opposite.

In addition, the reset level comparison in hns3_detect_reset_event()
is similar to the hns3vf_is_reset_pending(). So this patch fixes
above the problems and merges the logic of reset level comparison.

Fixes: 5be38fc6c0fc ("net/hns3: fix multiple reset detected log")
Cc: stable@dpdk.org

Signed-off-by: Dengdui Huang <huangdengdui@huawei.com>
Signed-off-by: Jie Hai <haijie1@huawei.com>
---
 drivers/net/hns3/hns3_ethdev.c | 22 +++++++---------------
 1 file changed, 7 insertions(+), 15 deletions(-)

diff --git a/drivers/net/hns3/hns3_ethdev.c b/drivers/net/hns3/hns3_ethdev.c
index 3de7a5f813..c7a2147f39 100644
--- a/drivers/net/hns3/hns3_ethdev.c
+++ b/drivers/net/hns3/hns3_ethdev.c
@@ -5545,27 +5545,15 @@ is_pf_reset_done(struct hns3_hw *hw)
 static enum hns3_reset_level
 hns3_detect_reset_event(struct hns3_hw *hw)
 {
-	struct hns3_adapter *hns = HNS3_DEV_HW_TO_ADAPTER(hw);
 	enum hns3_reset_level new_req = HNS3_NONE_RESET;
-	enum hns3_reset_level last_req;
 	uint32_t vector0_intr_state;
 
-	last_req = hns3_get_reset_level(hns, &hw->reset.pending);
 	vector0_intr_state = hns3_read_dev(hw, HNS3_VECTOR0_OTHER_INT_STS_REG);
 	if (BIT(HNS3_VECTOR0_IMPRESET_INT_B) & vector0_intr_state)
 		new_req = HNS3_IMP_RESET;
 	else if (BIT(HNS3_VECTOR0_GLOBALRESET_INT_B) & vector0_intr_state)
 		new_req = HNS3_GLOBAL_RESET;
 
-	if (new_req == HNS3_NONE_RESET)
-		return HNS3_NONE_RESET;
-
-	if (last_req == HNS3_NONE_RESET || last_req < new_req) {
-		__atomic_store_n(&hw->reset.disable_cmd, 1, __ATOMIC_RELAXED);
-		hns3_schedule_delayed_reset(hns);
-		hns3_warn(hw, "High level reset detected, delay do reset");
-	}
-
 	return new_req;
 }
 
@@ -5584,10 +5572,14 @@ hns3_is_reset_pending(struct hns3_adapter *hns)
 		return false;
 
 	new_req = hns3_detect_reset_event(hw);
+	if (new_req == HNS3_NONE_RESET)
+		return false;
+
 	last_req = hns3_get_reset_level(hns, &hw->reset.pending);
-	if (last_req != HNS3_NONE_RESET && new_req != HNS3_NONE_RESET &&
-	    new_req < last_req) {
-		hns3_warn(hw, "High level reset %d is pending", last_req);
+	if (last_req == HNS3_NONE_RESET || last_req < new_req) {
+		__atomic_store_n(&hw->reset.disable_cmd, 1, __ATOMIC_RELAXED);
+		hns3_schedule_delayed_reset(hns);
+		hns3_warn(hw, "High level reset detected, delay do reset");
 		return true;
 	}
 	last_req = hns3_get_reset_level(hns, &hw->reset.request);
-- 
2.41.0.windows.2

