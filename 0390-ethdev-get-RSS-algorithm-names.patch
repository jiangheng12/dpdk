From 811392906150ad09a2502b1d40f87cf48faec751 Mon Sep 17 00:00:00 2001
From: Jie Hai <haijie1@huawei.com>
Date: Thu, 2 Nov 2023 16:20:18 +0800
Subject: [PATCH 390/394] ethdev: get RSS algorithm names

[ upstream commit 92628e2b04923c098128acdb173ab25953162ef8 ]

This patch adds new API rte_eth_dev_rss_algo_name() to get
name of a RSS algorithm and document it.

Example:

testpmd> show port 0 rss-hash algorithm
RSS algorithm:
  toeplitz

Signed-off-by: Jie Hai <haijie1@huawei.com>
Acked-by: Huisong Li <lihuisong@huawei.com>
Acked-by: Chengwen Feng <fengchengwen@huawei.com>
Reviewed-by: Ferruh Yigit <ferruh.yigit@amd.com>
---
 app/test-pmd/cmdline.c                      | 29 +++++++++++++++++----
 app/test-pmd/config.c                       | 29 +++++++--------------
 app/test-pmd/testpmd.h                      |  2 +-
 doc/guides/testpmd_app_ug/testpmd_funcs.rst |  4 +--
 lib/ethdev/rte_ethdev.c                     | 25 ++++++++++++++++++
 lib/ethdev/rte_ethdev.h                     | 16 ++++++++++++
 lib/ethdev/version.map                      |  3 +++
 7 files changed, 81 insertions(+), 27 deletions(-)

diff --git a/app/test-pmd/cmdline.c b/app/test-pmd/cmdline.c
index 49152ec348..cdf943162b 100644
--- a/app/test-pmd/cmdline.c
+++ b/app/test-pmd/cmdline.c
@@ -174,8 +174,8 @@ static void cmd_help_long_parsed(void *parsed_result,
 			" by masks on port X. size is used to indicate the"
 			" hardware supported reta size\n\n"
 
-			"show port (port_id) rss-hash [key]\n"
-			"    Display the RSS hash functions and RSS hash key of port\n\n"
+			"show port (port_id) rss-hash [key | algorithm]\n"
+			"    Display the RSS hash functions, RSS hash key and RSS hash algorithms of port\n\n"
 
 			"clear port (info|stats|xstats|fdir) (port_id|all)\n"
 			"    Clear information for port_id, or all.\n\n"
@@ -3150,15 +3150,17 @@ struct cmd_showport_rss_hash {
 	cmdline_fixed_string_t rss_hash;
 	cmdline_fixed_string_t rss_type;
 	cmdline_fixed_string_t key; /* optional argument */
+	cmdline_fixed_string_t algorithm; /* optional argument */
 };
 
 static void cmd_showport_rss_hash_parsed(void *parsed_result,
 				__rte_unused struct cmdline *cl,
-				void *show_rss_key)
+				__rte_unused void *data)
 {
 	struct cmd_showport_rss_hash *res = parsed_result;
 
-	port_rss_hash_conf_show(res->port_id, show_rss_key != NULL);
+	port_rss_hash_conf_show(res->port_id,
+		!strcmp(res->key, "key"), !strcmp(res->algorithm, "algorithm"));
 }
 
 cmdline_parse_token_string_t cmd_showport_rss_hash_show =
@@ -3173,6 +3175,8 @@ cmdline_parse_token_string_t cmd_showport_rss_hash_rss_hash =
 				 "rss-hash");
 cmdline_parse_token_string_t cmd_showport_rss_hash_rss_key =
 	TOKEN_STRING_INITIALIZER(struct cmd_showport_rss_hash, key, "key");
+static cmdline_parse_token_string_t cmd_showport_rss_hash_rss_algo =
+	TOKEN_STRING_INITIALIZER(struct cmd_showport_rss_hash, algorithm, "algorithm");
 
 cmdline_parse_inst_t cmd_showport_rss_hash = {
 	.f = cmd_showport_rss_hash_parsed,
@@ -3189,7 +3193,7 @@ cmdline_parse_inst_t cmd_showport_rss_hash = {
 
 cmdline_parse_inst_t cmd_showport_rss_hash_key = {
 	.f = cmd_showport_rss_hash_parsed,
-	.data = (void *)1,
+	.data = NULL,
 	.help_str = "show port <port_id> rss-hash key",
 	.tokens = {
 		(void *)&cmd_showport_rss_hash_show,
@@ -3201,6 +3205,20 @@ cmdline_parse_inst_t cmd_showport_rss_hash_key = {
 	},
 };
 
+static cmdline_parse_inst_t cmd_showport_rss_hash_algo = {
+	.f = cmd_showport_rss_hash_parsed,
+	.data = NULL,
+	.help_str = "show port <port_id> rss-hash algorithm",
+	.tokens = {
+		(void *)&cmd_showport_rss_hash_show,
+		(void *)&cmd_showport_rss_hash_port,
+		(void *)&cmd_showport_rss_hash_port_id,
+		(void *)&cmd_showport_rss_hash_rss_hash,
+		(void *)&cmd_showport_rss_hash_rss_algo,
+		NULL,
+	},
+};
+
 /* *** Configure DCB *** */
 struct cmd_config_dcb {
 	cmdline_fixed_string_t port;
@@ -17899,6 +17917,7 @@ cmdline_parse_ctx_t main_ctx[] = {
 	(cmdline_parse_inst_t *)&cmd_tunnel_udp_config,
 	(cmdline_parse_inst_t *)&cmd_showport_rss_hash,
 	(cmdline_parse_inst_t *)&cmd_showport_rss_hash_key,
+	(cmdline_parse_inst_t *)&cmd_showport_rss_hash_algo,
 	(cmdline_parse_inst_t *)&cmd_config_rss_hash_key,
 	(cmdline_parse_inst_t *)&cmd_cleanup_txq_mbufs,
 	(cmdline_parse_inst_t *)&cmd_dump,
diff --git a/app/test-pmd/config.c b/app/test-pmd/config.c
index af00078108..9d7b10548e 100644
--- a/app/test-pmd/config.c
+++ b/app/test-pmd/config.c
@@ -1636,24 +1636,7 @@ rss_config_display(struct rte_flow_action_rss *rss_conf)
 		printf(" %d", rss_conf->queue[i]);
 	printf("\n");
 
-	printf(" function: ");
-	switch (rss_conf->func) {
-	case RTE_ETH_HASH_FUNCTION_DEFAULT:
-		printf("default\n");
-		break;
-	case RTE_ETH_HASH_FUNCTION_TOEPLITZ:
-		printf("toeplitz\n");
-		break;
-	case RTE_ETH_HASH_FUNCTION_SIMPLE_XOR:
-		printf("simple_xor\n");
-		break;
-	case RTE_ETH_HASH_FUNCTION_SYMMETRIC_TOEPLITZ:
-		printf("symmetric_toeplitz\n");
-		break;
-	default:
-		printf("Unknown function\n");
-		return;
-	}
+	printf(" function: %s\n", rte_eth_dev_rss_algo_name(rss_conf->func));
 
 	printf(" RSS key:\n");
 	if (rss_conf->key_len == 0) {
@@ -3077,7 +3060,7 @@ port_rss_reta_info(portid_t port_id,
  * key of the port.
  */
 void
-port_rss_hash_conf_show(portid_t port_id, int show_rss_key)
+port_rss_hash_conf_show(portid_t port_id, int show_rss_key, int show_rss_algo)
 {
 	struct rte_eth_rss_conf rss_conf = {0};
 	uint8_t rss_key[RSS_HASH_KEY_LENGTH];
@@ -3127,8 +3110,16 @@ port_rss_hash_conf_show(portid_t port_id, int show_rss_key)
 		printf("RSS disabled\n");
 		return;
 	}
+
+	if (show_rss_algo) {
+		printf("RSS algorithm:\n  %s\n",
+			rte_eth_dev_rss_algo_name(rss_conf.algorithm));
+		return;
+	}
+
 	printf("RSS functions:\n");
 	rss_types_display(rss_hf, TESTPMD_RSS_TYPES_CHAR_NUM_PER_LINE);
+
 	if (!show_rss_key)
 		return;
 	printf("RSS key:\n");
diff --git a/app/test-pmd/testpmd.h b/app/test-pmd/testpmd.h
index 30c7177630..d19deeff4a 100644
--- a/app/test-pmd/testpmd.h
+++ b/app/test-pmd/testpmd.h
@@ -1021,7 +1021,7 @@ int set_queue_rate_limit(portid_t port_id, uint16_t queue_idx, uint16_t rate);
 int set_vf_rate_limit(portid_t port_id, uint16_t vf, uint16_t rate,
 				uint64_t q_msk);
 
-void port_rss_hash_conf_show(portid_t port_id, int show_rss_key);
+void port_rss_hash_conf_show(portid_t port_id, int show_rss_key, int show_rss_algo);
 void port_rss_hash_key_update(portid_t port_id, char rss_type[],
 			      uint8_t *hash_key, uint8_t hash_key_len);
 int rx_queue_id_is_invalid(queueid_t rxq_id);
diff --git a/doc/guides/testpmd_app_ug/testpmd_funcs.rst b/doc/guides/testpmd_app_ug/testpmd_funcs.rst
index 50c45db6f7..a81296d2ba 100644
--- a/doc/guides/testpmd_app_ug/testpmd_funcs.rst
+++ b/doc/guides/testpmd_app_ug/testpmd_funcs.rst
@@ -233,9 +233,9 @@ size is used to indicate the hardware supported reta size
 show port rss-hash
 ~~~~~~~~~~~~~~~~~~
 
-Display the RSS hash functions and RSS hash key of a port::
+Display the RSS hash functions and RSS hash key or RSS hash algorithm of a port::
 
-   testpmd> show port (port_id) rss-hash [key]
+   testpmd> show port (port_id) rss-hash [key | algorithm]
 
 clear port
 ~~~~~~~~~~
diff --git a/lib/ethdev/rte_ethdev.c b/lib/ethdev/rte_ethdev.c
index ec06bd3a9c..289fe45e6c 100644
--- a/lib/ethdev/rte_ethdev.c
+++ b/lib/ethdev/rte_ethdev.c
@@ -196,6 +196,17 @@ enum {
 	STAT_QMAP_RX
 };
 
+static const struct {
+	enum rte_eth_hash_function algo;
+	const char *name;
+} rte_eth_dev_rss_algo_names[] = {
+	{RTE_ETH_HASH_FUNCTION_DEFAULT, "default"},
+	{RTE_ETH_HASH_FUNCTION_SIMPLE_XOR, "simple_xor"},
+	{RTE_ETH_HASH_FUNCTION_TOEPLITZ, "toeplitz"},
+	{RTE_ETH_HASH_FUNCTION_SYMMETRIC_TOEPLITZ, "symmetric_toeplitz"},
+	{RTE_ETH_HASH_FUNCTION_SYMMETRIC_TOEPLITZ_SORT, "symmetric_toeplitz_sort"},
+};
+
 int
 rte_eth_iterator_init(struct rte_dev_iterator *iter, const char *devargs_str)
 {
@@ -4288,6 +4299,20 @@ rte_eth_dev_rss_hash_conf_get(uint16_t port_id,
 								   rss_conf));
 }
 
+const char *
+rte_eth_dev_rss_algo_name(enum rte_eth_hash_function rss_algo)
+{
+	const char *name = "Unknown function";
+	unsigned int i;
+
+	for (i = 0; i < RTE_DIM(rte_eth_dev_rss_algo_names); i++) {
+		if (rss_algo == rte_eth_dev_rss_algo_names[i].algo)
+			return rte_eth_dev_rss_algo_names[i].name;
+	}
+
+	return name;
+}
+
 int
 rte_eth_dev_udp_tunnel_port_add(uint16_t port_id,
 				struct rte_eth_udp_tunnel *udp_tunnel)
diff --git a/lib/ethdev/rte_ethdev.h b/lib/ethdev/rte_ethdev.h
index 911b9e03ab..09a546a48b 100644
--- a/lib/ethdev/rte_ethdev.h
+++ b/lib/ethdev/rte_ethdev.h
@@ -4396,6 +4396,22 @@ int
 rte_eth_dev_rss_hash_conf_get(uint16_t port_id,
 			      struct rte_eth_rss_conf *rss_conf);
 
+/**
+ * @warning
+ * @b EXPERIMENTAL: this API may change, or be removed, without prior notice.
+ *
+ *  Get the name of RSS hash algorithm.
+ *
+ * @param rss_algo
+ *   Hash algorithm.
+ *
+ * @return
+ *   Hash algorithm name or 'UNKNOWN' if the rss_algo cannot be recognized.
+ */
+__rte_experimental
+const char *
+rte_eth_dev_rss_algo_name(enum rte_eth_hash_function rss_algo);
+
 /**
  * Add UDP tunneling port for a type of tunnel.
  *
diff --git a/lib/ethdev/version.map b/lib/ethdev/version.map
index f593f64ea9..1867016054 100644
--- a/lib/ethdev/version.map
+++ b/lib/ethdev/version.map
@@ -265,6 +265,9 @@ EXPERIMENTAL {
 	rte_eth_tx_descriptor_dump;
 	rte_eth_dev_is_valid_rxq;
 	rte_eth_dev_is_valid_txq;
+
+	# added in 23.11
+	rte_eth_dev_rss_algo_name;
 };
 
 INTERNAL {
-- 
2.23.0

