From bb21b8e295cf30381e12629096dd3f0ab7a47b6e Mon Sep 17 00:00:00 2001
From: Dongdong Liu <liudongdong3@huawei.com>
Date: Fri, 30 Jun 2023 15:37:37 +0800
Subject: doc: fix number of leading spaces in hns3 guide

[ upstream commit 620cd2d5ac5a6d7de750a4827e2840209a1b0cbb ]

The current description of 'mbx_time_limit_ms' has three spaces
at the beginning. Use two spaces to keep the same style with other
places and add a blank line after '::'.

Fixes: 2fc3e696a7f1 ("net/hns3: add runtime config for mailbox limit time")
Cc: stable@dpdk.org

Signed-off-by: Dongdong Liu <liudongdong3@huawei.com>
---
 doc/guides/nics/hns3.rst | 34 ++++++++++++++++++----------------
 1 file changed, 18 insertions(+), 16 deletions(-)

diff --git a/doc/guides/nics/hns3.rst b/doc/guides/nics/hns3.rst
index 644d520b64..8705845d8e 100644
--- a/doc/guides/nics/hns3.rst
+++ b/doc/guides/nics/hns3.rst
@@ -93,7 +93,7 @@ Runtime Configuration
 
   For example::
 
-  -a 0000:7d:00.0,rx_func_hint=simple
+    -a 0000:7d:00.0,rx_func_hint=simple
 
 - ``tx_func_hint`` (default ``none``)
 
@@ -114,7 +114,7 @@ Runtime Configuration
 
   For example::
 
-  -a 0000:7d:00.0,tx_func_hint=common
+    -a 0000:7d:00.0,tx_func_hint=common
 
 - ``dev_caps_mask`` (default ``0``)
 
@@ -127,22 +127,24 @@ Runtime Configuration
 
   For example::
 
-  -a 0000:7d:00.0,dev_caps_mask=0xF
+    -a 0000:7d:00.0,dev_caps_mask=0xF
 
 - ``mbx_time_limit_ms`` (default ``500``)
-   Used to define the mailbox time limit by user.
-   Current, the max waiting time for MBX response is 500ms, but in
-   some scenarios, it is not enough. Since it depends on the response
-   of the kernel mode driver, and its response time is related to the
-   scheduling of the system. In this special scenario, most of the
-   cores are isolated, and only a few cores are used for system
-   scheduling. When a large number of services are started, the
-   scheduling of the system will be very busy, and the reply of the
-   mbx message will time out, which will cause our PMD initialization
-   to fail. So provide access to set mailbox time limit for user.
-
-   For example::
-   -a 0000:7d:00.0,mbx_time_limit_ms=600
+
+  Used to define the mailbox time limit by user.
+  Current, the max waiting time for MBX response is 500ms, but in
+  some scenarios, it is not enough. Since it depends on the response
+  of the kernel mode driver, and its response time is related to the
+  scheduling of the system. In this special scenario, most of the
+  cores are isolated, and only a few cores are used for system
+  scheduling. When a large number of services are started, the
+  scheduling of the system will be very busy, and the reply of the
+  mbx message will time out, which will cause our PMD initialization
+  to fail. So provide access to set mailbox time limit for user.
+
+  For example::
+
+    -a 0000:7d:00.0,mbx_time_limit_ms=600
 
 - ``fdir_vlan_match_mode`` (default ``strict``)
 
-- 
2.23.0

