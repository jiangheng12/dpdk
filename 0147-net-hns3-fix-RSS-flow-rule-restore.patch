From c05520ce0dfe94fd8a676a4d69502f6abc67df08 Mon Sep 17 00:00:00 2001
From: Huisong Li <lihuisong@huawei.com>
Date: Fri, 21 Oct 2022 15:36:23 +0800
Subject: [PATCH 147/189] net/hns3: fix RSS flow rule restore

After reset process, types of RSS flow rule cannot be restored when
load driver without RTE_ETH_MQ_RX_RSS_FLAG flag. This is because the
restoration for RSS flow rule is done in the 'hns3_config_rss()'. But
this function is also used to configure and restore RSS configuration
from ethdev ops, and doesn't configure RSS types if 'rxmode.mq_mode'
has not the flag. As a result, RSS types configured by rte flow API
can't be restored in this case when encounter reset. Actually, all
RSS rules are saved to a global link list.

Use the linked list to restore RSS flow rule.

Fixes: 920be799dbc3 ("net/hns3: fix RSS indirection table configuration")
Cc: stable@dpdk.org

Signed-off-by: Huisong Li <lihuisong@huawei.com>
Signed-off-by: Dongdong Liu <liudongdong3@huawei.com>
---
 drivers/net/hns3/hns3_ethdev.c    | 11 ++---------
 drivers/net/hns3/hns3_ethdev_vf.c | 11 ++---------
 drivers/net/hns3/hns3_flow.c      |  8 +++++++-
 drivers/net/hns3/hns3_flow.h      |  1 +
 drivers/net/hns3/hns3_rss.h       |  1 -
 5 files changed, 12 insertions(+), 20 deletions(-)

diff --git a/drivers/net/hns3/hns3_ethdev.c b/drivers/net/hns3/hns3_ethdev.c
index 24ee9df332..fc3fc76a40 100644
--- a/drivers/net/hns3/hns3_ethdev.c
+++ b/drivers/net/hns3/hns3_ethdev.c
@@ -5006,6 +5006,7 @@ static int
 hns3_do_start(struct hns3_adapter *hns, bool reset_queue)
 {
 	struct hns3_hw *hw = &hns->hw;
+	struct rte_eth_dev *dev = &rte_eth_devices[hw->data->port_id];
 	bool link_en;
 	int ret;
 
@@ -5042,7 +5043,7 @@ hns3_do_start(struct hns3_adapter *hns, bool reset_queue)
 	if (ret)
 		goto err_set_link_speed;
 
-	return 0;
+	return hns3_restore_filter(dev);
 
 err_set_link_speed:
 	(void)hns3_cfg_mac_mode(hw, false);
@@ -5059,12 +5060,6 @@ hns3_do_start(struct hns3_adapter *hns, bool reset_queue)
 	return ret;
 }
 
-static void
-hns3_restore_filter(struct rte_eth_dev *dev)
-{
-	hns3_restore_rss_filter(dev);
-}
-
 static int
 hns3_dev_start(struct rte_eth_dev *dev)
 {
@@ -5121,8 +5116,6 @@ hns3_dev_start(struct rte_eth_dev *dev)
 	hns3_set_rxtx_function(dev);
 	hns3_mp_req_start_rxtx(dev);
 
-	hns3_restore_filter(dev);
-
 	/* Enable interrupt of all rx queues before enabling queues */
 	hns3_dev_all_rx_queue_intr_enable(hw, true);
 
diff --git a/drivers/net/hns3/hns3_ethdev_vf.c b/drivers/net/hns3/hns3_ethdev_vf.c
index db2f15abe2..13f1cba0e6 100644
--- a/drivers/net/hns3/hns3_ethdev_vf.c
+++ b/drivers/net/hns3/hns3_ethdev_vf.c
@@ -1727,6 +1727,7 @@ static int
 hns3vf_do_start(struct hns3_adapter *hns, bool reset_queue)
 {
 	struct hns3_hw *hw = &hns->hw;
+	struct rte_eth_dev *dev = &rte_eth_devices[hw->data->port_id];
 	uint16_t nb_rx_q = hw->data->nb_rx_queues;
 	uint16_t nb_tx_q = hw->data->nb_tx_queues;
 	int ret;
@@ -1741,13 +1742,7 @@ hns3vf_do_start(struct hns3_adapter *hns, bool reset_queue)
 	if (ret)
 		hns3_err(hw, "failed to init queues, ret = %d.", ret);
 
-	return ret;
-}
-
-static void
-hns3vf_restore_filter(struct rte_eth_dev *dev)
-{
-	hns3_restore_rss_filter(dev);
+	return hns3_restore_filter(dev);
 }
 
 static int
@@ -1799,8 +1794,6 @@ hns3vf_dev_start(struct rte_eth_dev *dev)
 	hns3_set_rxtx_function(dev);
 	hns3_mp_req_start_rxtx(dev);
 
-	hns3vf_restore_filter(dev);
-
 	/* Enable interrupt of all rx queues before enabling queues */
 	hns3_dev_all_rx_queue_intr_enable(hw, true);
 	hns3_start_tqps(hw);
diff --git a/drivers/net/hns3/hns3_flow.c b/drivers/net/hns3/hns3_flow.c
index 301a4a56b3..7bd2f0bf7a 100644
--- a/drivers/net/hns3/hns3_flow.c
+++ b/drivers/net/hns3/hns3_flow.c
@@ -1646,7 +1646,7 @@ hns3_clear_rss_filter(struct rte_eth_dev *dev)
 	return ret;
 }
 
-int
+static int
 hns3_restore_rss_filter(struct rte_eth_dev *dev)
 {
 	struct hns3_adapter *hns = dev->data->dev_private;
@@ -1672,6 +1672,12 @@ hns3_restore_rss_filter(struct rte_eth_dev *dev)
 	return ret;
 }
 
+int
+hns3_restore_filter(struct rte_eth_dev *dev)
+{
+	return hns3_restore_rss_filter(dev);
+}
+
 static int
 hns3_flow_parse_rss(struct rte_eth_dev *dev,
 		    const struct hns3_rss_conf *conf, bool add)
diff --git a/drivers/net/hns3/hns3_flow.h b/drivers/net/hns3/hns3_flow.h
index 1ab3f9f5c6..0f5de129a3 100644
--- a/drivers/net/hns3/hns3_flow.h
+++ b/drivers/net/hns3/hns3_flow.h
@@ -49,5 +49,6 @@ int hns3_dev_flow_ops_get(struct rte_eth_dev *dev,
 			  const struct rte_flow_ops **ops);
 void hns3_flow_init(struct rte_eth_dev *dev);
 void hns3_flow_uninit(struct rte_eth_dev *dev);
+int hns3_restore_filter(struct rte_eth_dev *dev);
 
 #endif /* _HNS3_FLOW_H_ */
diff --git a/drivers/net/hns3/hns3_rss.h b/drivers/net/hns3/hns3_rss.h
index 5b90d3a628..78c9eff827 100644
--- a/drivers/net/hns3/hns3_rss.h
+++ b/drivers/net/hns3/hns3_rss.h
@@ -110,6 +110,5 @@ int hns3_config_rss(struct hns3_adapter *hns);
 void hns3_rss_uninit(struct hns3_adapter *hns);
 int hns3_set_rss_tuple_by_rss_hf(struct hns3_hw *hw, uint64_t rss_hf);
 int hns3_rss_set_algo_key(struct hns3_hw *hw, const uint8_t *key);
-int hns3_restore_rss_filter(struct rte_eth_dev *dev);
 
 #endif /* _HNS3_RSS_H_ */
-- 
2.23.0

